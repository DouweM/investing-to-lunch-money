# investing-to-lunch-money

This [Meltano](https://www.meltano.com) project automatically syncs [Investing.com](https://investing.com) security prices to [Lunch Money](https://lunchmoney.app) asset balances using [`tap-investing`](https://gitlab.com/DouweM/tap-investing) and [`target-lunch-money`](https://gitlab.com/DouweM/target-lunch-money).

This project is built to run inside [GitLab CI](https://docs.gitlab.com/ee/ci/README.html) using [Pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html), but can also be [deployed elsewhere](https://meltano.com/docs/production.html).

## Demo

This GitLab project has been set up as described below and serves as the demo!

You can see the pipeline schedule and review the latest pipeline under [CI/CD > Pipeline schedules](https://gitlab.com/DouweM/investing-to-lunch-money/-/pipeline_schedules).

## Setup

In Lunch Money:

1. Under [Setup > Accounts](https://my.lunchmoney.app/accounts), create a manually-managed asset for each security you'd like to sync.
1. Under [Settings > Developers](https://my.lunchmoney.app/developers), create an API access token.

In GitLab:

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) this repository and perform the following actions inside your fork.
1. Make your fork [private](https://docs.gitlab.com/ee/public_access/public_access.html#private-projects).
1. Configure the assets you'd like to sync by editing the [`tap-investing` configuration](https://gitlab.com/DouweM/tap-investing#configuration) in [`meltano.yml`](./meltano.yml). Make sure the asset names here match those in Lunch Money.
1. Configure the Lunch Money API access token by adding a [GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/) with key `TARGET_LUNCH_MONEY_ACCESS_TOKEN` and the token as value.
1. Set up automatic syncing by adding a [GitLab CI pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) with a description and interval of your choice, e.g. "ELT" and "Every day at 9:00am".
1. Trigger the schedule manually or wait for it to trigger naturally.

Your investment asset prices will now be synced to Lunch Money automatically!
